import React from 'react';
import './Screen2.css';
import share from './share.svg';
import compare from './compare.svg';
import save from './save.svg';

function Screen2() {
  return (
	<div className='section col-sm-12 min-vh-90'>
		<h1 className='text-center font-weight-bolder text-black zebra-question mb-5'>How does The Zebra work?</h1>
		<div className='row pt-4 w-85 mx-auto'>
			<div className='div col-sm-12 col-md-4 col-lg-4 text-center'>
				<div className='images-div mx-auto mb-5'>
					<img src={share} className='info-image' alt='Share'/>
				</div>
				<h4 className='text-black font-weight-bolder'>Share</h4>
				<p className='info-text mx-auto px-xl-4'>Tell us a little about yourself and your car and home coverage needs.</p>
			</div>
			<div className='div col-sm-12 col-md-4 col-lg-4 text-center'>
				<div className='images-div mx-auto mb-5'>
				<img src={compare} className='info-image' alt='Compare'/>
				</div>
				<h4 className='text-black font-weight-bolder'>Compare</h4>
				<p className='info-text mx-auto px-xl-4'>We instantly show you dozens of quotes side-by-side from top providers for free.</p>
			</div>
			<div className='div col-sm-12 col-md-4 col-lg-4 text-center'>
				<div className='images-div mx-auto mb-5'>
				<img src={save} className='info-image' alt='Save'/>
				</div>
				<h4 className='text-black font-weight-bolder'>Save</h4>
				<p className='info-text mx-auto px-xl-4'>Choose your quote and secure your new rate in an instant, online or via one of our licensed agents.</p>
			</div>
		</div>
	</div>
  );
}

export default Screen2;
