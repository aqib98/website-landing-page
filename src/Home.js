import React from 'react';
// import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './Home.css';
import Nav from './Nav';
import Screen1 from './Screen1';
import Screen2 from './Screen2';
import Screen3 from './Screen3';
import Screen4 from './Screen4';
import Footer from './Footer';

function Home() {
  return (
      <div>
        <Nav />
        <Screen1 />
    		<Screen2 />
    		<Screen3 />
    		<Screen4 />
    		<Footer />
      </div>
  );
}

export default Home;
