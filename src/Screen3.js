import React from 'react';
import './Screen3.css';
import purpleStart from './purple-start.svg';
import purpleEnd from './purple-end.svg';
import privacy from './privacy.svg';

function Screen3() {
  return (
	<div className='col-sm-12 min-vh-90'>
		<div className='purple-section min-vh-100'>
		<img src={purpleStart} className='purple-line' alt='Privacy Info'/>
		<div className='privacy-info-div col-md-5 my-auto mx-auto text-center'>
			<img src={privacy} className='privacy-logo mb-4' alt='Privacy Logo'/>
			<p className='privacy-header text-white font-weight-bolder mb-2'>Our Privacy Pledge</p>
			<p className='privacy-brief text-white'>Your privacy matters to us. So does your sanity. We pledge not to sell your information to spammers, and that means no annoying calls, texts or emails. With The Zebra, compare and save the hassle-free way.</p>
		</div>
		</div>
		<img src={purpleEnd} className='purple-line' alt='Privacy Info'/>
	</div>
  );
}

export default Screen3;
