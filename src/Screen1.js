import React from 'react';
import './Screen1.css';
import openEnvelope from './open-envelope.svg';
import phone from './phone-icon.svg';
import user from './user-icon.svg';

function Screen1() {
  return (
	<div className='row min-vh-90 m-0'>
      <div className='sub-section-1 col-sm-12 col-lg-8 my-auto'>
      	<span className='heating-header-letter text-black font-weight-bold pb-4 ml-3'>H</span><span className='heating-header text-black font-weight-bold pb-4 pr-3'>eating & Cooling</span>
      	<p className='text-black mt-5 mb-0 col-sm-9'>When you run into trouble with your heating or air-conditioning system, you need someone you can trust! We have trained professionals ready to assist you day or night with our 24 hour emergency service.</p>
      </div>
      <div className='sub-section-2 col-sm-12 col-lg-4 my-auto'>
      	<div className='card mr-auto ml-auto'>
		    <div className='card-body text-center'>
				<h4 className='estimate-header text-uppercase font-weight-bold d-inline'>free 
				<h4 className='text-uppercase font-weight-bold d-inline estimate-border pb-2'> est</h4>imate</h4>
				<p className='estimate-text mt-3 mb-3'>Get a quote on replacing your unit today.</p>
				<form>
					<div className='form-row'>
						<div className='input-group mb-3'>
							<div className='input-group-prepend'>
								<input type='text' className='form-control border-right-0' placeholder='*Enter Your Name' />
								<span className='input-group-text bg-white border-left-0 pt-0 pb-0 pl-0'>
							    	<img src={user} className='user-icon' alt='User Icon'/>
							    </span>
							</div>
						</div>
						<div className='input-group mb-3'>
							<div className='input-group-prepend'>
								<input type='email' className='form-control border-right-0' placeholder='*Enter Your Email' />
								<span className='input-group-text bg-white border-left-0 pt-0 pb-0 pl-0'>
							    	<img src={openEnvelope} className='open-envelope-icon' alt='Open Envelope Icon'/>
							    </span>
							</div>
						</div>
						<div className='input-group mb-3'>
							<div className='input-group-prepend'>
								<input type='text' className='form-control border-right-0' placeholder='*Enter Your Phone' />
								<span className='input-group-text bg-white border-left-0 pt-0 pb-0 pl-0'>
							    	<img src={phone} className='phone-icon' alt='Phone Icon'/>
							    </span>
							</div>
						</div>
						<div className='estimate-quote-div'>
							<button type='button' className='estimate-quote-btn btn btn-primary text-uppercase'>Get quote ></button>
						</div>
					</div>
				</form>
		    </div>
		</div>
      </div>
	</div>
  );
}

export default Screen1;
