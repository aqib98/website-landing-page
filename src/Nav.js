import React from 'react';
import './Nav.css';
import call from './call.svg';

function Nav() {
  return (
	<nav className='navbar navbar-inverse'>
	  <div className='container-fluid p-2'>
	    <div className='navbar-header'>
	      <a className='navbar-brand text-black font-weight-bold' href='#'>BlitzKing</a>
	    </div>
	    <ul className='nav navbar-right'>
	    	<li className='dropdown px-4'><a className='nav-dropdown-text dropdown-toggle' data-toggle='dropdown' href='#'><span className='mr-2'>Compare</span></a>
		        <ul className='dropdown-menu'>
		          <li><a href='#'>Page 1-1</a></li>
		          <li><a href='#'>Page 1-2</a></li>
		          <li><a href='#'>Page 1-3</a></li>
		        </ul>
			</li>
	    	<li className='dropdown px-4'><a className='nav-dropdown-text dropdown-toggle' data-toggle='dropdown' href='#'><span className='mr-2'>Tools & Tips</span></a>
		        <ul className='dropdown-menu'>
		          <li><a href='#'>Page 1-1</a></li>
		          <li><a href='#'>Page 1-2</a></li>
		          <li><a href='#'>Page 1-3</a></li>
		        </ul>
			</li>
	    	<li className='dropdown px-4'><a className='nav-dropdown-text dropdown-toggle' data-toggle='dropdown' href='#'><span className='mr-2'>About Us</span></a>
		        <ul className='dropdown-menu'>
		          <li><a href='#'>Page 1-1</a></li>
		          <li><a href='#'>Page 1-2</a></li>
		          <li><a href='#'>Page 1-3</a></li>
		        </ul>
			</li>
			<div className='pl-3'>
				<img src={call} className='d-inline nav-call mb-0 mr-2' alt='Call'/>
				<p className='d-inline nav-call-number'>1.888.255.4364</p>
			</div>
	    </ul>
	  </div>
	</nav>
  );
}

export default Nav;
