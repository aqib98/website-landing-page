import React from 'react';
import './Screen4.css';
import online from './buy-online.svg';
import mobile from './mobile.svg';
import rightArrow from './right-arrow.svg';
import security from './security.svg';
import call from './call.svg';

function Screen4() {
  return (
	<div className='section col-sm-12 min-vh-90'>
	<p className='easy-header text-black font-weight-bolder text-center mb-5'>It's easy to start saving.</p>
		<div className='row mx-auto'>
			<div className=' col-sm-12 col-md-5 col-lg-5'>
				<div className='online-info-div col-sm-12 col-md-10 my-auto mx-auto text-center'>
					<img src={online} className='buy-online mb-3' alt='Buy Online'/>
					<p className='online-header text-black font-weight-bolder mb-2'>Buy Online</p>
					<p className='online-brief'>Our easy-to-use tool lets you compare quotes from top providers, all at once and online.</p>
				</div>
				<div className='col-sm-12 col-md-10 mx-auto'>
					<form className='form-inline'>
						<div className='form-group mx-auto mb-2'>
							<input type='text' className='start-input form-control input-lg' placeholder='Zip Code'/>		
							<button className='start-button p-4'>
								<span className='text-white font-weight-bolder'>Start </span>
								<img src={rightArrow} className='right-arrow m-0' alt='Buy Online'/>
							</button>
						</div>
					</form>
					<div className='d-flex align-items-center justify-content-center mx-auto'>
						<img src={security} className='security-svg mr-2' alt='Security'/>
						<p className='security-text m-0'>No spam. We take your privacy seriously.</p>
					</div>
				</div>
			</div>
			<div className='col-sm-12 col-md-2 col-lg-2'>
				<div class="cta-divider">
					<div class="cta-divider-circle">OR</div>
				</div>
			</div>
			<div className=' col-sm-12 col-md-5 col-lg-5'>
				<div className='online-info-div col-sm-12 col-md-10 my-auto mx-auto text-center'>
					<img src={mobile} className='call-mobile mb-3' alt='Mobile'/>
					<p className='online-header text-black font-weight-bolder mb-2'>Call an Agent</p>
					<p className='online-brief'>Whether you just prefer a friendly voice or need more information, our licensed agents are ready to help you save.</p>
					<div>
						<img src={call} className='d-inline call mb-0 mr-2' alt='Call'/>
						<p className='d-inline font-weight-bolder h4'>1.888.255.4364</p>
						<p className='call-brief font-weight-bolder'>Hablamos Español</p>
					</div>
				</div>
			</div>
		</div>
	</div>
  );
}

export default Screen4;
