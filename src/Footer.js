import React from 'react';
import './Footer.css';
import footerLine from './footer-line.svg';
import facebook from './facebook-logo.svg';
import twitter from './twitter-logo.svg';
import instagram from './instagram-logo.svg';
import linkedin from './linkedin-logo.svg';


function Footer() {
  return (
	<div>
		<img src={footerLine} className='footer-line' Alt='Footer'/>
		<div className='footer mb-0 pb-0'>
			<footer className='page-footer font-small indigo'>
				<div className='container text-center text-md-left'>
					<div className='footer-div row'>
						<div className='col-md-4 mx-auto'>
							<h5 className='font-weight-bold text-white mt-3 mb-4'>Insurance Guides</h5>
							<div className='footer-links'>
					            <p className='pb-2 m-0'>Liability Insurance</p> 
								<p className='pb-2 m-0'>Collision Insurance</p> 
								<p className='pb-2 m-0'>Comprehensive Insurance</p> 
								<p className='pb-2 m-0'>Uninsured Motorist Coverage</p> 
								<p className='pb-2 m-0'>Personal Injury Protection Insurance</p> 
								<p className='pb-2 m-0'>No-Fault Insurance</p> 
								<p className='pb-2 m-0'>Non-Owners Insurance</p> 
								<p className='pb-2 m-0'>Full Coverage Insurance</p> 
								<p className='pb-2 m-0'>Insurance Glossary</p> 
								<p className='pb-2 m-0'>Car Insurance Discounts</p> 
								<p className='pb-2 m-0'>Average Car Insurance</p> 
								<p className='pb-2 m-0'>Ask An Agent</p> 
								<p className='pb-2 m-0'>Reviews</p> 
								<p className='pb-2 m-0'>TV Commercials</p>
							</div>
						</div>
						<div class="col-md-8 mx-auto">
							<div className='col-sm-12'>
						        <h5 className='font-weight-bold text-white text-uppercase mt-3 mb-4'>Cheap Insurance Providers</h5>
						        <div className='row footer-links'>
						            <div className='d-inline col-sm-6 col-lg-3'>
						            	<p>USAA</p>
						            	<p>Progressive</p>
						            </div>
						            <div className='d-inline col-sm-6 col-lg-3'>
						            	<p>State Farm</p>
						            	<p>Esurance</p>
						            </div>
						            <div className='d-inline col-sm-6 col-lg-3'>
						            	<p>Farmers</p>
						            	<p>Nationwide</p>
						            </div>
						            <div className='d-inline col-sm-6 col-lg-3'>
						            	<p>MetLife</p>
						            	<p>21st Century</p>
						            </div>
						        </div>
							</div>
							<div className='col-sm-12'>
								<h5 className='font-weight-bold text-white text-uppercase mt-3 mb-4'>Find Cheap Insurance By State</h5>
								<div className='footer-links text-break'>
									<p className='d-inline mr-2 pb-2 mb-0'>AL</p>
									<p className='d-inline mr-2 pb-2 mb-0'>AK</p>
									<p className='d-inline mr-2 pb-2 mb-0'>AZ</p>
									<p className='d-inline mr-2 pb-2 mb-0'>AR</p>
									<p className='d-inline mr-2 pb-2 mb-0'>CA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>CO</p>
									<p className='d-inline mr-2 pb-2 mb-0'>CT</p>
									<p className='d-inline mr-2 pb-2 mb-0'>DE</p>
									<p className='d-inline mr-2 pb-2 mb-0'>FL</p>
									<p className='d-inline mr-2 pb-2 mb-0'>GA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>HI</p>
									<p className='d-inline mr-2 pb-2 mb-0'>ID</p>
									<p className='d-inline mr-2 pb-2 mb-0'>IL</p>
									<p className='d-inline mr-2 pb-2 mb-0'>IN</p>
									<p className='d-inline mr-2 pb-2 mb-0'>IA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>KS</p>
									<p className='d-inline mr-2 pb-2 mb-0'>KY</p>
									<p className='d-inline mr-2 pb-2 mb-0'>LA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>ME</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MD</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MI</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MN</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MS</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MO</p>
									<p className='d-inline mr-2 pb-2 mb-0'>MT</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NE</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NV</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NH</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NJ</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NM</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NY</p>
									<p className='d-inline mr-2 pb-2 mb-0'>NC</p>
									<p className='d-inline mr-2 pb-2 mb-0'>ND</p>
									<p className='d-inline mr-2 pb-2 mb-0'>OH</p>
									<p className='d-inline mr-2 pb-2 mb-0'>OK</p>
									<p className='d-inline mr-2 pb-2 mb-0'>OR</p>
									<p className='d-inline mr-2 pb-2 mb-0'>PA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>RI</p>
									<p className='d-inline mr-2 pb-2 mb-0'>SC</p>
									<p className='d-inline mr-2 pb-2 mb-0'>SD</p>
									<p className='d-inline mr-2 pb-2 mb-0'>TN</p>
									<p className='d-inline mr-2 pb-2 mb-0'>TX</p>
									<p className='d-inline mr-2 pb-2 mb-0'>UT</p>
									<p className='d-inline mr-2 pb-2 mb-0'>VT</p>
									<p className='d-inline mr-2 pb-2 mb-0'>VA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>WA</p>
									<p className='d-inline mr-2 pb-2 mb-0'>WV</p>
									<p className='d-inline mr-2 pb-2 mb-0'>WI</p>
									<p className='d-inline mr-2 pb-2 mb-0'>WY</p>
									<p className='d-inline mr-2 pb-2 mb-0'>| Cheap Insurance By City</p>
								</div>
							</div>
							<div className='col-sm-12'>
								<h5 className='font-weight-bold text-white text-uppercase mt-3 mb-4'>Compare Cheap Car Insurance Companies</h5>
								<div className='row footer-links'>
									<div className='d-inline col-sm-6 col-lg-3'>
										<p className="m-0 pb-2">Geico vs Progressive</p>
										<p className="m-0 pb-2">State Farm vs Allstate</p>
										<p className="m-0 pb-2">Geico vs State Farm</p>
										<p className="m-0 pb-2">Progressive vs State Farm</p>
									</div>
									<div className='d-inline col-sm-6 col-lg-3'>
										<p className="m-0 pb-2">AAA vs State Farm</p>
										<p className="m-0 pb-2">State Farm vs Farmers</p>
										<p className="m-0 pb-2">USAA vs Geico</p>
										<p className="m-0 pb-2">Geico vs Liberty Mutual</p>
									</div>
									<div className='d-inline col-sm-6 col-lg-3'>
										<p className="m-0 pb-2">AAA vs Geico</p>
										<p className="m-0 pb-2">Allstate vs AAA</p>
										<p className="m-0 pb-2">Geico vs Allstate</p>
										<p className="m-0 pb-2">State Farm vs Nationwide</p>
									</div>
									<div className='d-inline col-sm-6 col-lg-3'>
										<p className="m-0 pb-2">USAA vs State Farm</p>
										<p className="m-0 pb-2">AAA vs USAA</p>
										<p className="m-0 pb-2">Allstate vs Farmers</p>
										<p className="m-0 pb-2">State Farm vs Liberty Mutual</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className='row'>
						<div className='col-sm-12'>
							<ul className='footer-ul text-center m-0'>
								<li className='footer-li text-white mx-3 d-inline-block'>
									<p>About Us</p>
								</li>
								<li className='footer-li text-white mx-3 d-inline-block'>
									<p>Press</p>
								</li>
								<li className='footer-li text-white mx-3 d-inline-block'>
									<p className='hiring-box'>We Are Hiring</p>
								</li>
								<li className='footer-li text-white mx-3 d-inline-block'>
									<p>Contact Us</p>
								</li>
							</ul>
						</div>
						<div className='col-sm-12'>
						</div>
					</div>
					<div className='text-center'>
						<p class="text-grey mb-1">Follow us on:</p>
						<div className='d-inline'>
							<img src={facebook} className='facebook-icon m-2' alt='Facebook' />
							<img src={twitter} className='twitter-icon m-2' alt='Twitter' />
							<img src={instagram} className='instagram-icon m-2' alt='Instagram' />
							<img src={linkedin} className='linkedin-icon m-2' alt='Linkedin' />
						</div>
					</div>	
				</div>
				<div className='container'>
					<div className='footer-copyright text-center py-3 col-sm-8 mx-auto'>
						<p>© 2019 Insurance Zebra. All Rights Reserved. Use of Insurance Zebra Insurance Services (DBA TheZebra.com) is subject to our <span className='font-weight-bold'>Terms of Service</span>, <span className='font-weight-bold'>Privacy Policy</span> and <span className='font-weight-bold'>Licenses</span>.</p>
					</div>
				</div>
			</footer>
		</div>
	</div>
  );
}

export default Footer;
